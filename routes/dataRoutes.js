const express = require('express'),
      router = express.Router(),
      dataValidator = require('../middlewares/validators/dataValidator'),
      DataController = require('../controllers/dataController')

router.get('/', DataController.getAll)
router.get('/:id', dataValidator.getOne, DataController.getOne)

module.exports = router
