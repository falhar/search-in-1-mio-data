const mongoose = require('mongoose');

const DataSchema = new mongoose.Schema({
  details: {
    asks: {type: Array, require:true},
    bids: {type: Array, require:true},
    lag: {type: Number, require:true},
    system: {type: String, require:true}
  },
  price: {
    type: Number,
    require: true
  },
  shares: {
    type: Number,
    required: true
  },
  ticker: {
    type:String,
    required:false
  },
  ticket: {
    type: String,
    required: false
  }
}, {
  timestamps: {
    time: 'created_at'
  },
  versionKey: false
});



module.exports = data = mongoose.model('trades', DataSchema);
