const mongoose = require('mongoose');

const uri = "mongodb+srv://firman:mongofirman123@sample-data.wtsd3.mongodb.net/sample_training?retryWrites=true&w=majority"

mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
});

const data = require('./data')


module.exports = {
  data
}
