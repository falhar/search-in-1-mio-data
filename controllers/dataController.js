const { data } = require('../models')

class DataController {
  async getAll(req, res) {
    try {
      const {
        page = 1, limit = 10
      } = req.query;

      const theData = await data.find().limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();

      const count = await data.countDocuments();


      res.json({
        theData,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      })
    } catch (e) {
      res.json({
        status: 'Error!',
        error: e
      })
    }
  }
  async getOne(req, res) {
    try {
      data.findOne({
        _id: req.params.id
      }).then(result => {
        return res.json({
          status: 'Success!',
          data: result
        })
      })
    } catch (e) {
      res.json({
        status: 'Error!',
        error: e
      })
    }
  }
}

module.exports = new DataController
