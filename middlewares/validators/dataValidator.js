const { check, matchedData, sanitize, validationResult } = require('express-validator'),
      { data } = require('../../models')

module.exports = {
  getOne: [
    check('id').isLength({min:24, max:24}).withMessage('invalid object id')
    .isString().custom(value => {
      return data.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('data doesn\'t exist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ]
}
