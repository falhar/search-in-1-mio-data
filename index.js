const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      dataRoutes = require('./routes/dataRoutes')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended:true
}))

app.use('/', dataRoutes)

app.listen(3000, () => {
  console.log('Server running on port 3000')
})
